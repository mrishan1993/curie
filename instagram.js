const puppeteer = require('puppeteer');
const BASE_URL = 'https://instagram.com/'
const TAG_URL = 'https://instagram.com/explore/tags/'
const FOLLOW_URL = 'https://www.instagram.com/sec.tor.29/'

const instagram = {
    browser : null,
    page : null,

    initialize : async () => {
        instagram.browser = await puppeteer.launch ( {
            headless : false
        })
        instagram.page = await instagram.browser.newPage()
        
    },

    // for logging in
    login : async (username, password) => {
        await instagram.page.goto(BASE_URL, {waitUntil : 'networkidle2'})


        // wait for the page to be loaded
        await instagram.page.waitFor(1000)
        // write the username and password
        await instagram.page.type('input[name="username"', username, {delay : 50})
        await instagram.page.type('input[name="password"', password, {delay : 50})
        // click the login button 
        let loginButton = await instagram.page.$x('//div[contains(text(),"Log In")]')
        console.log("loginButton", loginButton)
        await loginButton[0].click()

        // wait for the page to load
        await instagram.page.waitFor('div[role="presentation"]')
    },

    // for liking posts with tags
    likeTagsProcess : async (tags = []) => {
        for (let tag of tags) {
            try {
                await instagram.page.goto(TAG_URL + tag + "/", {waitUntil : 'networkidle2'})    
            } catch (error) {
                console.log("error", error)
            }
            
            await instagram.page.waitFor(1000)
            // select the most recent posts
            let posts = await instagram.page.$$('article > div:nth-child(3) img[decoding="auto"]')
            for (let iCount=0;iCount<3 && iCount < posts.length;iCount++) {
                let post = posts[iCount]
                await post.click()
                // await instagram.page.waitFor('span[id="react-root"][aria-hidden = "true"]')
                await instagram.page.waitFor(3000)

                let isLikeable = await instagram.page.$('svg[aria-label="Like"]')
                if (isLikeable) {
                    await instagram.page.click('svg[aria-label="Like"]')   
                }
                await instagram.page.click('svg[aria-label="Close"]')
                await instagram.page.waitFor(3000)
            }
        }
    },

    // follow people through tags
    followPeopleThroughTags : async (tags = []) => {
        var totalFollows = 0
        for (let tag of tags) {
            try {
                await instagram.page.goto(TAG_URL + tag + "/", {waitUntil : 'networkidle2'})    
            } catch (error) {
                console.log("error", error)
            }
            
            await instagram.page.waitFor(1000)
            // select the most recent posts
            let posts = await instagram.page.$$('article > div:nth-child(3) img[decoding="auto"]')
            for (let iCount=0;iCount<1 && iCount < posts.length;iCount++) {
                let post = posts[iCount]
                await post.click()
                // await instagram.page.waitFor('span[id="react-root"][aria-hidden = "true"]')
                await instagram.page.waitFor(3000)
                // let loginButton = await instagram.page.$x('//div[contains(text(),"Log In")]')
                let isFollowable = await instagram.page.$x('/html/body/div[4]/div[2]/div/article/header/div[2]/div[1]/div[2]/button')
                if (isFollowable) {
                    await isFollowable[0].click()
                    totalFollows = totalFollows + 1
                }
                await instagram.page.click('svg[aria-label="Close"]')
                await instagram.page.waitFor(3000)
            }
        }

        // for unfollowing everyone you just followed 
        await unfollowRecent(totalFollows)
    },
    // unfollow recent people 
    unfollowRecent : async (count = 0) => {
        try {
            await instagram.page.goto(FOLLOW_URL, {waitUntil : 'networkidle2'})    
        } catch (error) {
            console.log("error", error)
        }
        await instagram.page.waitFor(1000)
        let clickFollowing = await instagram.page.$x('//*[@id="react-root"]/section/main/div/header/section/ul/li[3]/a')
        await clickFollowing[0].click()
        await instagram.page.waitFor(1000)
        await instagram.page.waitFor('ul')
        for (var iCount=0;iCount<count;iCount++) {
            path = iCount + 1 + ""
            let isUnfollowable = await instagram.page.$$('ul > div > li:nth-child(' + path + ') > div > div > button')
            if (isUnfollowable) {
                console.log("here 5", isUnfollowable.target)
                await isUnfollowable[0].click()
                let confirmButton = await instagram.page.$x('/html/body/div[5]/div/div/div[3]/button[1]')
                await confirmButton[0].click()
            }
        }
        await instagram.page.click('svg[aria-label="Close"]')
        await instagram.page.waitFor(3000)
    },
}

module.exports = instagram